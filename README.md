# bsc-task

BSC test exercise

Maven start:
mvn spring-boot:run -Dspring-boot.run.arguments=[coma,separated,strings]

To build fat jar:
mvn package

To run jar:
java -jar target/bsc-1.0-SNAPSHOT.jar [args]

The Application accepts only upper ASCII characters in Currency Code.
The Application accepts only integer Amount in Java Integer parsable format.
Delimiter is "Space" character only.
CLI invalid input prevents the Application from running.
Console and file invalid input is ignored.
Console and file input is retyped for clean reading.
Configuration is placed in code constants.
Implementation is supposed to be thread safe. But there is no atomic input/output guaranties.
No optimization for extra large input files was in scope.
Only Unit tests are implemented. There are neither end to end nor integration tests.
