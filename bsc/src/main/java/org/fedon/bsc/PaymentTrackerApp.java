package org.fedon.bsc;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

import org.fedon.bsc.tracker.PaymentTracker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class PaymentTrackerApp implements CommandLineRunner {
    private static final String quit = "quit";

    @Autowired
    private PaymentTracker tracker;

    public static void main(String[] args) throws Exception {
        SpringApplication.run(PaymentTrackerApp.class, args);
    }

    public void run(String... ins) throws Exception {
        if (ins.length == 1 && ins[0].startsWith("@")) {
            try {
                readFile(ins[0].substring(1));
            } catch (FileNotFoundException e) {
                System.out.println(e.getMessage());
                System.out.println("Skipping...");
            }
        } else {
            tracker.track(ins);
        }

        readSTDIN();
    }

    private void readSTDIN() {
        Scanner scan = new Scanner(System.in);
        while (true) {
            String str = scan.nextLine();
            if (quit.equals(str)) {
                scan.close();
                System.out.println("Application is going down.");
                System.exit(0);
            } else {
                readLine(str);
            }
        }
    }

    private void readFile(String fileName) throws FileNotFoundException {
        Scanner scan = new Scanner(new File(fileName));
        while (scan.hasNextLine()) {
            readLine(scan.nextLine());
        }
        scan.close();
    }

    private void readLine(String line) {
        try {
            tracker.track(line);
        } catch (IllegalArgumentException e) {
            System.out.println(e.getMessage());
            System.out.println("Ignoring...");
        }
    }
}