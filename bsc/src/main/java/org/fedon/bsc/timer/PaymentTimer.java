package org.fedon.bsc.timer;

import org.fedon.bsc.tracker.PaymentTracker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * @author Dmytro Fedonin
 *
 */
@Component
public class PaymentTimer {
    @Autowired
    private PaymentTracker tracker;

    @Scheduled(fixedRate = 60000)
    public void report() {
        System.out.println("*****");
        tracker.getPayments().entrySet().forEach(e -> {
            if (e.getValue() != 0) {
                System.out.println(e.getKey() + " " + e.getValue());
            }
        });
        System.out.println("=====\n");
    }
}
