package org.fedon.bsc.tracker;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.springframework.stereotype.Component;

/**
 * @author Dmytro Fedonin
 *
 */
@Component
public class PaymentTracker {
    private static final int codeLength = 3;
    private static final int amountStrart = codeLength + 1;
    private Map<String, Integer> payments = new ConcurrentHashMap<>();

    public void track(String... strings) {
        System.out.println("in ***");
        for (String str : strings) {
            System.out.println("* " + str);
            updatePayments(parseCurrencyCode(str), parseAmount(str));
        }
    }

    private String parseCurrencyCode(String str) {
        if (str != null && str.length() > codeLength) {
            String code = str.substring(0, codeLength);
            validateCode(code);
            return code;
        }
        throw new IllegalArgumentException("Input is too short");
    }

    private Integer parseAmount(String str) {
        // check delimiter
        if (str.charAt(codeLength) == ' ') {
            return Integer.parseInt(str.substring(amountStrart));
        }
        throw new IllegalArgumentException("Unexpected character at position " + codeLength + ". Must be 'Space'.");
    }

    private void validateCode(String code) {
        for (int i = 0; i < code.length(); i++) {
            if (code.charAt(i) < 'A' || 'Z' < code.charAt(i)) {
                throw new IllegalArgumentException("Currency code may contain only upper ASCII letters. Error character was found at position " + i);
            }
        }
    }

    private void updatePayments(String code, Integer amount) {
        if (payments.containsKey(code)) {
            payments.put(code, payments.get(code) + amount);
        } else {
            payments.put(code, amount);
        }
    }

    public Map<String, Integer> getPayments() {
        return payments;
    }
}
