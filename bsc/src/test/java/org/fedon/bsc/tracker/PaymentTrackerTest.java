package org.fedon.bsc.tracker;

import static org.junit.Assert.assertTrue;

import java.util.Map;

import org.junit.Test;

/**
 * @author Dmytro Fedonin
 *
 */
public class PaymentTrackerTest {
    PaymentTracker tracker = new PaymentTracker();
    String code = "AAA";
    String shortCode = "AA";
    String illCode = "AAa";
    int amount = 10;

    @Test
    public void track() {
        tracker.track(code + " " + amount);

        Map<String, Integer> map = tracker.getPayments();
        assertTrue(map.containsKey(code));
        assertTrue(map.get(code) == amount);
    }

    @Test
    public void track_Update() {
        tracker.track(code + " " + amount);
        tracker.track(code + " " + amount);

        Map<String, Integer> map = tracker.getPayments();
        assertTrue(map.containsKey(code));
        assertTrue(map.get(code) == amount * 2);
    }

    @Test(expected = IllegalArgumentException.class)
    public void track_BadCode() {

        tracker.track(illCode + " " + amount);
    }

    @Test(expected = IllegalArgumentException.class)
    public void track_ShortCode() {

        tracker.track(shortCode + " " + amount);
    }

    @Test(expected = IllegalArgumentException.class)
    public void track_Short() {

        tracker.track(shortCode);
    }
    @Test(expected = IllegalArgumentException.class)
    public void track_BadAmount() {

        tracker.track(code + " q");
    }

    @Test(expected = IllegalArgumentException.class)
    public void track_BadDelimiter() {

        tracker.track(code + "w" + amount);
    }
}
